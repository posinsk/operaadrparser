<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
    </body>
</html>
<?php

class operaAdrParser
{

    private $file;
    private $lines;
    private $objects;

    public function __construct($path)
    {
        $this->file = file_get_contents($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if (!$this->file)
            throw new Exception('file not loaded');
        
        $this->getLines();
    }

    private function getLines()
    {

        $lines = explode("\n", $this->file);
//       $lines = explode("\n\r" , $this->file);
        
        foreach($lines as $line):
            $line = trim($line);
            if(!empty($line))
                $this->lines[] = $line;
        endforeach;
        
        $this->cleanLines();
        
        $this->separateObjects($this->lines);
        
        $this->removeFolders();
        
        $this->formatObjects();
    }

    /**
     * Method  erases unwanted lines from file
     */
    private function cleanLines()
    {
        //This is for the first two lines of file
        unset($this->lines[0]);
        unset($this->lines[1]);
        
        $forbidden = array('ID=', 'VISITED=', 'ON PERSONALBAR=', 'PERSONALBAR_POS=', 'UNIQUEID=', 'DISPLAY URL =', 'PARTNERID=', 'DISPLAY URL=');
        foreach ($this->lines as $i => $line):
            foreach ($forbidden as $phrase):
                if (strpos($line, $phrase) === 0 || $line === '-')
                    unset($this->lines[$i]);
            endforeach;
        endforeach;
    }
           
    private function separateObjects()
    {
        
        $itemCounter = 0;
        $tree = array();
        foreach ($this->lines as $i => $line):
            
            if ($line === '#URL' || $line === '#FOLDER')
                $itemCounter++;
            
            $tree[$itemCounter][] = $line;
            unset($this->lines[$i]);
        endforeach;
        
        $this->objects = $tree;
    }
    
    private function removeFolders(){
        foreach($this->objects as $i =>$one):
            if($one[0] === '#FOLDER')
                unset($this->objects[$i]);
        endforeach;
    }
    
    private function formatObjects()
    {
        foreach ($this->objects as $i => $one):
            unset($this->objects[$i][0]);
            foreach ($one as $j => $line):
                if(strpos($line, 'NAME=') === 0)
                        $this->posts[$i]['title'] = substr($line, 5);
                elseif(strpos($line, 'URL=') === 0)
                        $this->posts[$i]['url'] = substr($line, 4);
                elseif(strpos($line, 'DESCRIPTION=') === 0)
                        $this->posts[$i]['notes'] = substr($line, 12);
                elseif(strpos($line, 'CREATED=') === 0)
                        $this->posts[$i]['created_at'] = date('H:i:s d-m-Y', substr($line, 8));
            endforeach;
        endforeach;
    }

    public function getPosts()
    {
        return $this->posts;
    }
    
    public function getObjects(){
        return $this->objects;
    }

}

$f = new operaAdrParser('op.adr');

foreach($f->getPosts() as $o):
    var_dump($o);
endforeach;


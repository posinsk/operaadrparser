<?php

//this is just for purpose of setting HTTP headers without any HTML
header('Content-Type: text/html; charset=utf-8');

class netscapeBookmarkParser
{

    /**
     * Parsed html document body
     * @var DOMDocument object
     */
    private $docBody;
    
    /**
     * Parsed links
     * @var array
     */
    private $links;

    /**
     * This is the public constructor of the class. It allows you to chain method right
     * after creating the object
     * @param type $path
     * @return netscapeBookmarkParser object (chainable)
     */
    public static function inititate($path)
    {
        return new netscapeBookmarkParser($path);
    }

    /**
     * constructor is private, but it does everything that is necessary to parse
     * the file and show its results. In this method the file is parsed.
     * @param type $path
     * @return netscapeBookmarkParser 
     */
    private function __construct($path)
    {
        if (!class_exists('DOMDocument'))
            throw new Exception('DOMDocument class undefined');

        $file = file_get_contents($path);

        $docBody = new DOMDocument();
        @$docBody->loadHTML($file);

        $this->docBody = $docBody;

        $this->parse();

        return $this;
    }

    /**
     * Method searches the document for existence of anchors and parses their attributes and value.
     */
    private function parse()
    {

        $p = $this->docBody->getElementsByTagName('a');

        $links = array();

        foreach ($p as $i => $one):
            $links[$i]['url'] = $one->getAttribute('href');
            $links[$i]['title'] = $one->nodeValue;
            $links[$i]['created_at'] = date('Y-m-d H:i:s', $one->getAttribute('add_date'));
        endforeach;

        $this->links = $links;
    }

    /**
     * Standard var getter. It returns parsed links with url,title,created_at
     * @return array array('url', 'title', 'created_at');
     */
    public function getLinks()
    {
        return $this->links;
    }

}

/**
 * Class example use:
 * 
 * 
 *                   CREATE THE CLASS OBJECT            GET PARSED LINKS 
 * $instance = netscapeBookmarkParser::inititate('b.htm')->getLinks();
 * 
 * 
 * var_dump($instance);
 */